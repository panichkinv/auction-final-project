/* global describe it before after */

const chai = require('chai');
const chaiHttp = require('chai-http');
const mocha = require('mocha');
const { expect } = require('chai');
const config = require('config');
const server = require('../../server');

chai.use(chaiHttp);

module.exports = () => {
    before(done => {
        server.startServer(done);
    });
    describe('Start Server', () => {
        it('it should return json with statusAuctionCode 200 if server is OK', done => {
            chai.request(`http://localhost:${config.get('Server.port')}`)
                .get('/testServer')
                .end((err, res) => {
                    expect(res.body.statusAuctionCode).to.equal('200');
                    done();
                });
        });
    });
    after(done => {
        server.stopServer(done);
    });
};

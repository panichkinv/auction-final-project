/* global describe it before after */

const chai = require('chai');
const chaiHttp = require('chai-http');
const mocha = require('mocha');
const { expect } = require('chai');
const config = require('config');
const sinon = require('sinon');

const Users = require('../../src/backEnd/Users/index');

chai.use(chaiHttp);

const testVariables = {
    login: 'testLogin'
};

// TODO find a way to set req.session.userId

function routesTests() {
    describe('Routes test', () => {
        describe('Login', () => {
            it("login:it should return json with statusAuctionCode 400 when login and password haven't been passed", done => {
                chai.request(`http://localhost:${config.get('Server.port')}`)
                    .post('/login')
                    .end((err, res) => {
                        expect(res.body.statusAuctionCode).to.equal('400');
                        done();
                    });
            });
            it("login: it should return correct data field with user's login when login and password provided", done => {
                const usersStub = sinon.stub(Users, 'loginOrSignUp');
                usersStub.resolves({ userId: '123', login: testVariables.login });
                chai.request(`http://localhost:${config.get('Server.port')}`)
                    .post('/login')
                    .send({
                        password: 'testPassword',
                        login: testVariables.login
                    })
                    .end((err, res) => {
                        expect(res.body.statusAuctionCode).to.equal('200');
                        expect(res.body.data).to.equal(testVariables.login);
                        usersStub.restore();
                        done();
                    });
            });
        });
        describe('Login Session', () => {
            it("it should return json with statusAuctionCode 400 whether req.session.userId haven't been passed", done => {
                chai.request(`http://localhost:${config.get('Server.port')}`)
                    .post('/loginSession')
                    .end((err, res) => {
                        expect(res.body.statusAuctionCode).to.equal('400');
                        done();
                    });
            });
        });
        describe('Logout', () => {
            it('logout: it should return json with statusAuctionCode 403 whether user is not logged in', done => {
                chai.request(`http://localhost:${config.get('Server.port')}`)
                    .post('/logout')
                    .end((err, res) => {
                        expect(res.body.statusAuctionCode).to.equal('403');
                        done();
                    });
            });
        });
    });
}

module.exports = routesTests;

/* eslint-disable no-underscore-dangle */
const responses = require('./responses');
const Lots = require('./Lots/index.js');
const Users = require('./Users/index.js');

const loginActions = {
    loginSession(req, res) {
        if (req.session.userId) {
            Users.sessionLogin(req.session.userId)
                .then(user => {
                    if (user) {
                        responses.sendOK(res, user.login);
                    } else {
                        responses.wrongAuth(res);
                    }
                })
                .catch(e => {
                    console.error(e);
                });
        } else {
            responses.wrongParams(res);
        }
    },
    signInUp(req, res) {
        const { login, password } = req.body;
        if (login && password) {
            Users.loginOrSignUp(login, password)
                .then(userData => {
                    req.session.userId = userData._id;
                    responses.sendOK(res, userData.login);
                })
                .catch(e => responses.wrongAuth(res, e.message));
        } else {
            responses.wrongParams(res);
        }
    },
    logout(req, res) {
        const { login } = req.currentUser;
        const { session } = req;
        if (login && session) {
            session.destroy();
            responses.sendOK(res);
        } else {
            responses.wrongParams(res);
        }
    }
};

const auctionActions = {
    increaseBid(req, res) {
        const { bidId } = req.body;
        const { login } = req.currentUser;
        if (login && bidId) {
            Lots.bidLotUp(login, bidId)
                .then(() => responses.sendOK(res))
                .catch(e => responses.notReady(res, e));
        } else {
            responses.wrongParams(res);
        }
    },
    addNewLots(req, res) {
        const { caption, price } = req.body;
        const currentBid = parseInt(req.body.currentBid, 10);
        const owner = req.currentUser.login;
        const timeToFinish = new Date(
            +req.body.year,
            +req.body.month - 1,
            +req.body.day,
            +req.body.hours,
            +req.body.minutes
        );
        if (req.file && timeToFinish > Date.now()) {
            const image = {
                url: `${req.file.destination.slice(req.file.destination.indexOf('images'))}/${req.file.filename}`,
                alt: `${req.file.originalname.slice(0, req.file.originalname.lastIndexOf('.'))} lot image`
            };
            if (caption && currentBid && price && owner && timeToFinish) {
                Lots.insertLot({ caption, currentBid, price, image, timeToFinish, owner, currentWinner: '' })
                    .then(() => responses.sendOK(res))
                    .catch(e => responses.notReady(res, e));
            } else {
                responses.wrongParams(res);
            }
        } else {
            responses.wrongParams(res);
        }
    }
};

const serverActions = {
    testServer(req, res) {
        responses.sendOK(res);
        res.end();
    }
};

module.exports = {
    ...serverActions,
    ...loginActions,
    ...auctionActions
};

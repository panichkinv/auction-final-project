const Database = require('../MongoDB');
const Security = require('../Security');
const responses = require('../responses');

const dbCollection = 'users';

const Users = {
    isUserExisted(userName) {
        return new Promise((resolve, reject) => {
            Database.find(dbCollection, { login: userName })
                .then(res => {
                    resolve(!!res.length);
                })
                .catch(e => reject(e));
        });
    },
    login(userName, password) {
        return new Promise((resolve, reject) => {
            Database.find(dbCollection, { login: userName })
                .then(user => Promise.all([user, Security.checkPassword(password, user[0].hash)]))
                .then(res => {
                    if (res[1]) {
                        resolve(res[0][0]);
                    } else {
                        reject(new Error('Cannot authorize with login and password'));
                    }
                })
                .catch(e => reject(e));
        });
    },
    signUp(userName, password) {
        const newUser = {
            login: userName
        };
        return new Promise((resolve, reject) => {
            Security.hashPassword(password)
                .then(hash => {
                    newUser.hash = hash;
                    return Database.insert(dbCollection, newUser);
                })
                .then(() => resolve(newUser))
                .catch(e => reject(e));
        });
    },
    loginOrSignUp(userName, password) {
        return new Promise((resolve, reject) => {
            Users.isUserExisted(userName)
                .then(isUser => {
                    if (isUser) {
                        return Users.login(userName, password);
                    }
                    return Users.signUp(userName, password);
                })
                .then(user => {
                    resolve(user);
                })
                .catch(e => reject(e));
        });
    },
    sessionLogin(id) {
        return new Promise((resolve, reject) => {
            Database.find(dbCollection, { _id: Database.objectIdWrap(id) })
                .then(user => {
                    if (user.length > 0) {
                        resolve(user[0]);
                    } else {
                        reject();
                    }
                })
                .catch(e => reject(e));
        });
    },
    checkUserMiddleWare(req, res, next) {
        if (req.session.userId) {
            Users.sessionLogin(req.session.userId)
                .then(user => {
                    if (user) {
                        req.currentUser = user;
                        next();
                    } else {
                        responses.wrongAuth(res);
                    }
                })
                .catch(e => {
                    console.error(e);
                });
        } else {
            responses.wrongAuth(res);
        }
    }
};

module.exports = Users;

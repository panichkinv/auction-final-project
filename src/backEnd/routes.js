/* eslint-disable no-underscore-dangle */
const Lots = require('./Lots/index.js');
const Users = require('./Users/index.js');
const actions = require('./actions');

/** @module auction/app */

function routes(app, server, upload) {
    /**
     *  @method check Server availability
     *  @description /testServer - Path to server
     *  @returns {Json} Return json with  "statusAuctionCode" : "200" if everything is OK
     */

    app.all('/testServer', actions.testServer);

    /**
     *  @method try to login user if he is returning
     *  @description /loginSession - Path to server
     *  @returns {Json} Return json with "statusAuctionCode" : "200" if user's session exists
     */

    app.post('/loginsession', actions.loginSession);

    /**
     *  @method login/signup user
     *  @description /login - Path to server
     *  @param {string} login - User login
     *  @param {string} password - User password
     *  @returns {Json} Return json with "statusAuctionCode" : "200" including new or existing user if the password is correct
     */

    app.post('/login', actions.signInUp);

    /**
     *  @method logout user
     *  @description /logout - Path to server
     *  @param {object} session - Session object must be provided
     *  @returns {Json} Return json with  "statusAuctionCode" : "200"
     *  if use successfully logout
     */

    app.post('/logout', Users.checkUserMiddleWare, actions.logout);

    /**
     *  @method increase the bid
     *  @description /bidup - Path to server
     *  @param {string} bidId - Unique identifier of the bid
     *  @returns {Json} Return json with  "statusAuctionCode" : "200"
     *  if successfully increase the bid
     */

    app.post('/bidUp', Users.checkUserMiddleWare, actions.increaseBid);

    /**
     *  @method add lots to Auction
     *  @description /addLotsToAuction - Path to server
     *  @param {string} caption - The caption of the lot.
     *  @param {number} year,month,day,hours,minutes - these are parameters for lot expiration date
     *  @param {string} price - The price of the current lot.
     *  @param {number} currentBid - initial bid to be set.
     *  @returns {Json} Return json with  "statusAuctionCode" : "200"
     *  if successfully added new lot
     */

    app.post('/addLotsToAuction', Users.checkUserMiddleWare, upload.single('lotImage'), actions.addNewLots);

    // eslint-disable-next-line global-require
    const io = require('socket.io')(server);
    io.on('connection', socket => {
        function sendDataToRerender() {
            Lots.sendLotsViaSocket(socket);
        }
        Lots.sendLotsViaSocket(socket);
        Lots.needRefreshEmit.on('someLotsUpdated', sendDataToRerender);
        const serverDateInterval = setInterval(() => {
            socket.emit('serverDate', Date.now());
        }, 1000);
        socket.on('disconnect', () => {
            clearInterval(serverDateInterval);
            Lots.needRefreshEmit.removeListener('someLotsUpdated', sendDataToRerender);
        });
    });
}

module.exports = routes;

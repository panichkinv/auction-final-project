function findMongoDB(db, collectionName, query) {
    return new Promise((resolve, reject) => {
        const collection = db.collection(collectionName);
        collection.find(query).toArray((err, docs) => {
            if (err) {
                reject(err);
            }
            resolve(docs);
        });
    }).catch(e => console.error(e.message));
}

module.exports = findMongoDB;

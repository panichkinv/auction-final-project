/* eslint-disable global-require */
const config = require('config');
const { MongoClient } = require('mongodb');

const url = config.get('Database.url');
let db;

function connectToDB() {
    return new Promise((resolve, reject) => {
        MongoClient.connect(
            url,
            (err, client) => {
                if (err) {
                    reject(err);
                }
                db = client;
                resolve();
            }
        );
    }).catch(e => {
        throw new Error(`DB Connection error: ${e.message}`);
    });
}

module.exports.connectDB = connectToDB;
module.exports.getDB = () => db;
module.exports.disconnectDB = () => {
    db.close();
};

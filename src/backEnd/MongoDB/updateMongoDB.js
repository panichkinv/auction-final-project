function updateMongoDB(db, collectionName, query, newData, extraConditions) {
    return new Promise((resolve, reject) => {
        const collection = db.collection(collectionName);
        collection.update(query, newData, extraConditions, (err, result) => {
            if (err) {
                reject(err);
            }
            resolve(result);
        });
    }).catch(e => console.error(e.message));
}

module.exports = updateMongoDB;

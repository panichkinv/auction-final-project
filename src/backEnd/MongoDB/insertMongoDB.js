function insertMongoDB(db, collectionName, insertData) {
    return new Promise((resolve, reject) => {
        const collection = db.collection(collectionName);
        collection.insert(insertData, (err, result) => {
            if (err) {
                reject(err);
            }
            resolve(result);
        });
    }).catch(e => console.error(e.message));
}

module.exports = insertMongoDB;

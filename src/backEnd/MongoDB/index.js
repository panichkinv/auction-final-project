const { ObjectId } = require('mongodb');

const connectToDb = require('./connectToDB');
const insertMongoDB = require('./insertMongoDB');
const findMongoDB = require('./findMongoDB');
const updateMongoDB = require('./updateMongoDB');
const deleteMongoDB = require('./deleteMongoDB');
const dbName = require('config').get('Database.dbName');

const Database = {
    insert(collection, insertData) {
        return new Promise((resolve, reject) => {
            insertMongoDB(connectToDb.getDB().db(dbName), collection, insertData)
                .then(res => {
                    resolve(res);
                })
                .catch(e => reject(e));
        }).catch(e => console.error('MongoDB.insert reporting - ', e.message));
    },
    update(collection, query, newData, extraConditions = {}) {
        return new Promise((resolve, reject) => {
            updateMongoDB(connectToDb.getDB().db(dbName), collection, query, newData, extraConditions)
                .then(res => {
                    resolve(res);
                })
                .catch(e => reject(e));
        }).catch(e => console.error('MongoDB.update reporting - ', e.message));
    },
    find(collection, query = {}) {
        return new Promise((resolve, reject) => {
            findMongoDB(connectToDb.getDB().db(dbName), collection, query)
                .then(res => {
                    resolve(res);
                })
                .catch(e => reject(e));
        }).catch(e => console.error('MongoDB.find reporting - ', e.message));
    },
    delete(collection, query, extraConditions = {}) {
        return new Promise((resolve, reject) => {
            deleteMongoDB(connectToDb.getDB().db(dbName), collection, query, extraConditions)
                .then(res => {
                    resolve(res);
                })
                .catch(e => reject(e));
        }).catch(e => console.error('MongoDB.delete reporting - ', e.message));
    },
    objectIdWrap(id) {
        return new ObjectId(id);
    }
};

module.exports = Database;

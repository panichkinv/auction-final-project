function deleteMongoDB(db, collectionName, query, extraConditions) {
    return new Promise((resolve, reject) => {
        const collection = db.collection(collectionName);
        collection.remove(query, extraConditions, (err, docs) => {
            if (err) {
                reject(err);
            }
            resolve(docs);
        });
    }).catch(e => console.error(e.message));;
}

module.exports = deleteMongoDB;

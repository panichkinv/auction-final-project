const bcrypt = require('bcrypt');

const Security = {
    hashPassword(plainTextPass, saltRounds = 10) {
        return new Promise((resolve, reject) => {
            bcrypt.hash(plainTextPass, saltRounds).then(hash => {
                if (hash) {
                    resolve(hash);
                }
                reject(new Error('Hash is not valid'));
            });
        }).catch(e => {
            console.error(`Encryption Error: ${e.message}`);
        });
    },
    checkPassword(plainTextPass, hash) {
        return new Promise(resolve => {
            bcrypt.compare(plainTextPass, hash).then(res => {
                resolve(res);
            });
        }).catch(e => {
            console.error(`Encryption Error: ${e.message}`);
        });
    }
};

module.exports = Security;

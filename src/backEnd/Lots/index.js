const Database = require('../MongoDB');
const EventEmitter = require('events');

const lotsCollection = 'lots';

class NeedRefreshEmitter extends EventEmitter {}

const needRefreshEmit = new NeedRefreshEmitter();

needRefreshEmit.setMaxListeners(500);

const Lots = {
    getLotsByQuery(query) {
        return new Promise((resolve, reject) => {
            Database.find(lotsCollection, query)
                .then(res => {
                    resolve(res);
                })
                .catch(e => reject(e));
        });
    },
    insertLot(lot) {
        return new Promise((resolve, reject) => {
            Database.insert(lotsCollection, lot)
                .then(res => {
                    needRefreshEmit.emit('someLotsUpdated');
                    resolve(res);
                })
                .catch(e => reject(e));
        });
    },
    sendLotsViaSocket(socket) {
        return new Promise((resolve, reject) => {
            Lots.getLotsByQuery({})
                .then(res => {
                    socket.emit('lots', res);
                    resolve();
                })
                .catch(e => reject(e));
        });
    },
    bidLotUp(userName, lotId) {
        return new Promise((resolve, reject) => {
            Database.update(
                lotsCollection,
                { _id: Database.objectIdWrap(lotId) },
                {
                    $set: {
                        currentWinner: userName
                    },
                    $inc: {
                        currentBid: 1
                    }
                }
            )
                .then(() => {
                    needRefreshEmit.emit('someLotsUpdated');
                    resolve();
                })
                .catch(e => reject(e));
        });
    },
    needRefreshEmit
};

module.exports = Lots;

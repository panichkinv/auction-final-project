// 404 no found
module.exports.notFound = res =>
    res.json({
        statusAuctionCode: '404'
    });

// 200 ok
module.exports.sendOK = (res, data) =>
    res.json({
        statusAuctionCode: '200',
        data
    });

// 403 bad request wrong Params
module.exports.notReady = (res, errorMsg) => {
    if (errorMsg) {
        res.json({
            errorAuctionMsg: errorMsg,
            statusAuctionCode: '403'
        });
    } else {
        res.json({
            statusAuctionCode: '403'
        });
    }
};
module.exports.wrongAuth = module.exports.notReady;

module.exports.wrongParams = (res, errorMsg) => {
    if (errorMsg) {
        res.json({
            errorAuctionMsg: errorMsg,
            statusAuctionCode: '400'
        });
    } else {
        res.json({
            statusAuctionCode: '400'
        });
    }
};

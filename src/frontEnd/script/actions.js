/* eslint-disable no-underscore-dangle,no-use-before-define,no-param-reassign */
import axios from './axios.min';
import Gl from './serviceVariables';

function menu() {
    let leavedCategories = true;
    let leavedSubmenu = true;
    let timeout;

    const config = {
        submenuId: 'categoriesSubMenu',
        submenuOpenedClass: 'nav__sublist_opened',
        categoriesId: 'categories',
        mobile: {
            hamburgerClass: 'hamburger',
            hamburgerClassActive: 'hamburger_active',
            navClass: 'mobile-nav',
            navClassActive: 'mobile-nav_active'
        },
        menuItems: {
            contactsId: 'contacts'
        }
    };

    function openMenu(event) {
        clearTimeout(timeout);
        if (event.target.id === config.submenuId) {
            leavedSubmenu = false;
        }
        if (event.target.id === config.categoriesId) {
            leavedCategories = false;
        }
        if (
            document
                .getElementsByClassName(config.mobile.hamburgerClass)[0]
                .classList.contains(config.mobile.hamburgerClassActive)
        ) {
            document.getElementById(config.menuItems.contactsId).style.paddingTop = '50px';
        }

        document.getElementById(config.submenuId).classList.add(config.submenuOpenedClass);
    }

    function closeMenu(event) {
        if (event.currentTarget !== config.submenuId) {
            leavedSubmenu = true;
        }
        if (event.currentTarget !== config.categoriesId) {
            leavedCategories = true;
        }
        if (leavedCategories && leavedSubmenu) {
            document.getElementById(config.submenuId).classList.remove(config.submenuOpenedClass);
            timeout = setTimeout(() => {
                if (!document.getElementById(config.submenuId).classList.contains(config.submenuOpenedClass)) {
                    document.getElementById(config.menuItems.contactsId).style.paddingTop = null;
                }
            }, 1000);
        }
    }

    function toggleMobileMenu() {
        if (window.innerWidth <= 800) {
            document
                .getElementsByClassName(config.mobile.hamburgerClass)[0]
                .classList.toggle(config.mobile.hamburgerClassActive);
            if (document.getElementsByTagName('BODY')[0].style.overflow) {
                document.getElementsByTagName('BODY')[0].style.overflow = '';
            } else {
                document.getElementsByTagName('BODY')[0].style.overflow = 'hidden';
            }

            if (document.getElementsByClassName(config.mobile.hamburgerClass)[0].style.padding) {
                document.getElementsByClassName(config.mobile.hamburgerClass)[0].style.padding = '';
            } else {
                document.getElementsByClassName(config.mobile.hamburgerClass)[0].style.padding =
                    '16px 16px 200vh 200vw';
            }
            setTimeout(() => {
                document
                    .getElementsByClassName(config.mobile.navClass)[0]
                    .classList.toggle(config.mobile.navClassActive);
            }, 200);
        }
    }

    function toggleMobileSubMenu(event) {
        event.stopPropagation();
        if (this.nextElementSibling.style.maxHeight) {
            this.nextElementSibling.style.maxHeight = '';
        } else {
            this.nextElementSibling.style.maxHeight = '500px';
        }
    }

    return {
        openMenu,
        closeMenu,
        toggleMobileMenu,
        toggleMobileSubMenu
    };
}

function slider() {
    let currentIndex = 1;
    let intervalId;
    let timeoutId;

    const config = {
        slider: {
            imageWrap: 'slider__image-wrapper',
            active: 'slider__image-wrapper_active'
        },
        dots: {
            item: 'nav-dots-wrapper__dot',
            active: 'nav-dots-wrapper__dot_active'
        }
    };

    function autoSlideShow() {
        intervalId = setInterval(() => {
            currentSlide(null, (currentIndex += 1));
        }, 5000);
    }

    function currentSlide(event, n) {
        // retrieve number of slide to be shown - n variable
        if (event !== null) {
            clearInterval(intervalId);
            // eslint-disable-next-line no-param-reassign
            n = parseInt(event.target.id.slice(event.target.id.indexOf('_') + 1), 10);
        }
        clearTimeout(timeoutId);
        if (isNaN(n)) return;
        currentIndex = n;
        const slides = document.getElementsByClassName(config.slider.imageWrap);
        const dots = document.getElementsByClassName(config.dots.item);
        if (n > slides.length) {
            currentIndex = 1;
        } else if (n < 1) {
            currentIndex = slides.length;
        }
        for (let i = 0; i < slides.length; i += 1) {
            if (currentIndex - 1 === i) {
                slides[i].classList.add(config.slider.active);
                dots[i].className += ` ${config.dots.active}`;
            } else {
                slides[i].classList.remove(config.slider.active);
                dots[i].className = dots[i].className.replace(config.dots.active, '');
            }
        }
        if (event !== null) timeoutId = setTimeout(autoSlideShow, 3000);
    }

    autoSlideShow();

    return currentSlide;
}

function liftDots(event) {
    const config = {
        className: {
            dots: {
                original: 'nav-dots-wrapper',
                up: 'nav-dots-wrapper_up'
            },
            textBox: {
                original: 'mainInfo__textbox',
                down: 'mainInfo__textbox_fade-out'
            }
        }
    };
    if (event.type === 'mouseenter') {
        document.getElementsByClassName(config.className.dots.original)[0].classList.add(config.className.dots.up);
        document
            .getElementsByClassName(config.className.textBox.original)[0]
            .classList.add(config.className.textBox.down);
    }
    if (event.type === 'mouseleave') {
        document.getElementsByClassName(config.className.dots.original)[0].classList.remove(config.className.dots.up);
        document
            .getElementsByClassName(config.className.textBox.original)[0]
            .classList.remove(config.className.textBox.down);
    }
}

const login = {
    _config: {
        text: {
            login: 'Вход',
            logout: 'Выход'
        },
        classNames: {
            popup: 'dialog-popup',
            nav: {
                list: 'nav__list',
                queryLink: '.nav__link',
                mobile: {
                    list: 'mobile-nav__list',
                    queryLink: '.mobile-nav__link'
                }
            },
            errorMessage: 'errorMessage'
        },
        id: {
            login: 'loginEntry'
        }
    },
    toggleLoginPopUp(event, insideCall) {
        event.stopPropagation();
        if (event.target.textContent === login._config.text.login || insideCall === true) {
            if (document.getElementsByClassName(login._config.classNames.popup)[0].style.display) {
                document.getElementsByClassName(login._config.classNames.popup)[0].style.display = '';
                document.removeEventListener('click', login.closeDialogClickOutTheBox);
            } else {
                document.getElementsByClassName(login._config.classNames.popup)[0].style.display = 'flex';
                document.addEventListener('click', login.closeDialogClickOutTheBox);
            }
        }
        if (event.target.textContent === login._config.text.logout) {
            axios
                .post('/logout', { login: Gl.userName })
                .then(() => {
                    Gl.loggedIn = false;
                    login.changeNavLastItem();
                    addLots.toggleAddLotsButton();
                    addLots.toggleDialogPopUp(null, false);
                })
                .catch(e => {
                    console.error(e);
                });
        }
    },
    closeDialogClickOutTheBox(e) {
        if (!document.getElementsByClassName(login._config.classNames.popup)[0].contains(e.target)) {
            login.toggleLoginPopUp(e, true);
        }
    },
    changeNavLastItem() {
        const entryLinks = [
            document
                .getElementsByClassName(login._config.classNames.nav.mobile.list)[0]
                .lastElementChild.querySelector(login._config.classNames.nav.mobile.queryLink),
            document
                .getElementsByClassName(login._config.classNames.nav.list)[0]
                .lastElementChild.querySelector(login._config.classNames.nav.queryLink)
        ];
        for (let i = 0; i < entryLinks.length; i += 1) {
            entryLinks[i].textContent =
                entryLinks[i].textContent === login._config.text.login
                    ? login._config.text.logout
                    : login._config.text.login;
        }
    },
    signIn(event) {
        function showError(text, parentElem) {
            const p = document.createElement('p');
            p.className = login._config.classNames.errorMessage;
            p.textContent = text;
            parentElem.appendChild(p);
            setTimeout(() => {
                parentElem.removeChild(p);
            }, 2000);
        }

        if (event.target.id === login._config.id.login) {
            const form = event.target.parentNode;
            const sendObj = {};
            let filled = true;
            for (let i = 0; i < form.length - 1; i += 1) {
                if (form.elements[i].value === '') {
                    filled = false;
                }
                sendObj[form.elements[i].name] = form.elements[i].value;
            }
            if (filled) {
                event.target.style.visibility = 'hidden';
                axios
                    .post('/login', sendObj)
                    .then(response => {
                        if (response.data.errorAuctionMsg) {
                            showError(response.data.errorAuctionMsg, form);
                        } else {
                            Gl.loggedIn = true;
                            Gl.userName = response.data.data;
                            login.toggleLoginPopUp(event, true);
                            login.changeNavLastItem();
                            addLots.toggleAddLotsButton();
                        }
                        event.target.style.visibility = '';
                    })
                    .catch(e => {
                        console.error(e);
                    });
            }
        }
    },
    trySignInWithSession() {
        axios
            .post('/loginSession')
            .then(response => {
                if (response.data.statusAuctionCode === '200') {
                    Gl.loggedIn = true;
                    Gl.userName = response.data.data;
                    login.changeNavLastItem();
                    addLots.toggleAddLotsButton();
                }
            })
            .catch(e => {
                console.error(e);
            });
    }
};

const pagination = {
    _config: {
        classNames: {
            auction: 'auction-wrapper',
            pagination: 'pagination-wrapper',
            goods: 'separate-goods',
            paginationLink: 'pagination-wrapper__item',
            paginationLinkActive: 'pagination-wrapper__item_active'
        },
        settings: {
            initialPage: 1,
            pageLength: 10
        }
    },
    render() {
        const totalRecords = auction.lots.length;
        const pages = Math.ceil(totalRecords / actions.pagination._config.settings.pageLength);
        for (let i = 0; i < pages; i += 1) {
            const a = document.createElement('a');
            a.className = actions.pagination._config.classNames.paginationLink;
            a.onclick = actions.pagination.goToPage.bind(this, i + 1);
            a.textContent = i + 1;
            document.getElementsByClassName(pagination._config.classNames.pagination)[0].appendChild(a);
        }
        pagination.goToPage(pagination.currentPage || 1);
    },
    goToPage(pageNum) {
        filter.clearFilter();
        const pagButtons = document.querySelectorAll(`.${actions.pagination._config.classNames.paginationLink}`);
        for (let i = 0; i < pagButtons.length; i += 1) {
            pagButtons[i].classList.remove(actions.pagination._config.classNames.paginationLinkActive);
            if (pageNum - 1 === i) {
                pagButtons[i].classList.add(actions.pagination._config.classNames.paginationLinkActive);
            }
        }
        const pageData = actions.auction.getPageData(pageNum, actions.pagination._config.settings.pageLength);
        actions.auction.pageData = pageData;
        pagination.currentPage = pageNum;
        document.getElementsByClassName(actions.pagination._config.classNames.auction)[0].innerHTML = '';
        pageData.map(lot =>
            actions.auction.renderLot(
                lot.timeToFinish,
                lot.caption,
                lot.image.url,
                lot.image.alt,
                lot.price,
                lot.currentBid,
                lot.currentWinner,
                lot._id
            )
        );
        actions.auction.startAuctionTimer(pageData);
    },
    goToLastPage() {
        const totalRecords = auction.lots.length;
        const lastPage = Math.ceil(totalRecords / actions.pagination._config.settings.pageLength);
        pagination.goToPage(lastPage);
    }
};

const auction = {
    _config: {
        classNames: {
            auction: 'auction-wrapper',
            queryGood: '.separate-good',
            pagination: 'pagination-wrapper',
            separateLot: 'auction-wrapper__element separate-good',
            timeField: {
                queryOriginal: '.service-info__current-time',
                finishing: 'service-info__current-time_timeout',
                finished: 'service-info__current-time_ended'
            },
            bidButton: {
                finishing: 'service-info__bid-button_timeout',
                finished: 'service-info__bid-button_ended',
                finishedText: 'Завершено',
                readyToBidText: 'Ставка'
            }
        }
    },
    _presentFormatedDate(date) {
        if (date instanceof Date) {
            const hours = (date.valueOf() / 1000 / 60 / 60).toFixed(0);
            const minutes = date.getMinutes();
            const seconds = date.getSeconds();
            return `${hours >= 10 ? hours : `0${hours}`}:${minutes >= 10 ? minutes : `0${minutes}`}:${
                seconds >= 10 ? seconds : `0${seconds}`
            }`;
        }
        return `${date.slice(0, 10)} ${new Date(date).toLocaleTimeString().slice(0, -3)}`;
    },
    initialfillAuctionLots(data) {
        auction.lots = [];
        document.getElementsByClassName(auction._config.classNames.auction)[0].innerHTML = '';
        data.forEach(element => {
            auction.lots.push(element);
        });
        document.getElementsByClassName(auction._config.classNames.pagination)[0].innerHTML = '';
        pagination.render();
    },
    startAuctionTimer(lotsArr) {
        lotsArr = lotsArr || auction.lots;
        const lots = Array.prototype.slice.call(document.querySelectorAll(auction._config.classNames.queryGood));
        clearInterval(auction.intervalId);
        auction.intervalId = setInterval(() => {
            lots.forEach((lot, index) => {
                const currentTimeElement = lot.querySelector(auction._config.classNames.timeField.queryOriginal);
                const newTime = new Date(Date.parse(lotsArr[index].timeToFinish) - Gl.serverDate);
                if (newTime > 0) {
                    currentTimeElement.textContent = auction._presentFormatedDate(newTime);
                } else {
                    currentTimeElement.textContent = auction._presentFormatedDate(lotsArr[index].timeToFinish);
                    currentTimeElement.classList.remove(auction._config.classNames.timeField.finishing);
                    currentTimeElement.nextElementSibling.classList.remove(
                        auction._config.classNames.bidButton.finishing
                    );
                    currentTimeElement.classList.add(auction._config.classNames.timeField.finished);
                    currentTimeElement.nextElementSibling.classList.add(auction._config.classNames.bidButton.finished);
                    currentTimeElement.nextElementSibling.value = auction._config.classNames.bidButton.finishedText;
                }
                if (newTime < 10000 && newTime > 0) {
                    currentTimeElement.classList.add(auction._config.classNames.timeField.finishing);
                    currentTimeElement.nextElementSibling.classList.toggle(
                        auction._config.classNames.bidButton.finishing
                    );
                }
            });
        }, 1000);
    },
    bidMoney(event) {
        if (event.target.value === auction._config.classNames.bidButton.readyToBidText) {
            if (Gl.loggedIn) {
                axios
                    .post('/bidUp', { login: Gl.userName, bidId: event.target.dataset.bidId })
                    .then(() => {})
                    .catch(e => {
                        console.error('Cannot bid due to the error: ', e.message);
                    });
            } else {
                login.toggleLoginPopUp(event, true);
            }
        }
    },
    renderLot(timeToFinish, caption, imageUrl, imageAlt, price, currentBid, currentWinner, id) {
        const div = document.createElement('div');
        div.className = auction._config.classNames.separateLot;
        let extraBidClass = '';
        let extraTimeClass = '';
        let extraBidValue = 'Ставка';
        let initialTime = new Date(new Date(timeToFinish) - Gl.serverDate);
        if (initialTime < 0) {
            initialTime = timeToFinish;
            extraBidClass = auction._config.classNames.bidButton.finished;
            extraTimeClass = auction._config.classNames.timeField.finished;
            extraBidValue = auction._config.classNames.bidButton.finishedText;
        }
        div.innerHTML =
            `  <h3 class="separate-good__caption">${caption}</h3>` +
            `            <div class="separate-good__image-wrapper">` +
            `                <img src="${imageUrl}" alt="${imageAlt}" ` +
            `    class="separate-good__image">` +
            `            </div>` +
            `            <p class="separate-good__price">\u0426\u0435\u043D\u0430: ${price}$</p>` +
            `            <div class="separate-good__service-info service-info">` +
            `                <div class="service-info__service-line">` +
            `                    <p class="service-info__current-bid">${currentBid}$</p>` +
            `                    <p class="service-info__current-winner">${currentWinner}</p>` +
            `                </div>` +
            `                <p class="service-info__current-time ${extraTimeClass}">${auction._presentFormatedDate(
                initialTime
            )}</p>` +
            `                <input type="button" class="service-info__bid-button ${extraBidClass}" data-bid-id="${id}" value="${extraBidValue}"/>` +
            `            </div>`;
        document.getElementsByClassName(auction._config.classNames.auction)[0].appendChild(div);
    },
    getPageData(pageNum, pageLength) {
        const startItemOnPage = (pageNum - 1) * pageLength;
        const endItemOnPage = startItemOnPage + pageLength;
        return auction.lots.slice(startItemOnPage, endItemOnPage);
    }
};

const filter = {
    _config: {
        className: {
            auction: 'auction-wrapper',
            search: 'search-wrapper__search-field'
        },
        checked: {
            radioButton: 'radio-wrapper__element',
            checkBox: 'checkbox-wrapper__element'
        }
    },
    howFilter: {
        won() {
            return this.filter(lot => new Date(Date.parse(lot.timeToFinish) - Gl.serverDate) < 0);
        },
        inProgress() {
            return this.filter(lot => new Date(Date.parse(lot.timeToFinish) - Gl.serverDate) > 0);
        },
        maxPrice() {
            return this.sort((a, b) => b.price - a.price);
        },
        maxTimeLeft() {
            return this.sort(
                (a, b) =>
                    new Date(Date.parse(b.timeToFinish) - Gl.serverDate) -
                    new Date(Date.parse(a.timeToFinish) - Gl.serverDate)
            );
        },
        minTimeLeft() {
            return this.sort(
                (a, b) =>
                    new Date(Date.parse(a.timeToFinish) - Gl.serverDate) -
                    new Date(Date.parse(b.timeToFinish) - Gl.serverDate)
            );
        },
        minBid() {
            return this.sort((a, b) => a.currentBid - b.currentBid);
        },
        search(query) {
            return this.filter(lot => lot.caption.toLowerCase().indexOf(query.toLowerCase()) !== -1);
        }
    },
    specifyActiveLabels() {
        const activeLabels = [];
        const radioButtons = document.getElementsByClassName(filter._config.checked.radioButton);
        const checkBoxes = document.getElementsByClassName(filter._config.checked.checkBox);
        const search = document.getElementsByClassName(filter._config.className.search)[0];
        for (let i = 0; i < radioButtons.length; i += 1) {
            if (radioButtons[i].checked) {
                activeLabels.push(radioButtons[i].getAttribute('value'));
            }
        }
        for (let i = 0; i < checkBoxes.length; i += 1) {
            if (checkBoxes[i].checked) {
                activeLabels.push(checkBoxes[i].getAttribute('value'));
            }
        }
        if (search.value) {
            activeLabels.push('search');
        }
        return activeLabels;
    },
    applyFilter(event) {
        if (event.target.tagName === 'INPUT') {
            const activeLabels = filter.specifyActiveLabels(event.target);
            const renderArray = activeLabels.reduce((renArray, currentFilter) => {
                if (filter.howFilter[currentFilter]) {
                    return filter.howFilter[currentFilter].call(renArray, event.target.value);
                }
                return renArray;
            }, auction.pageData.slice());
            document.getElementsByClassName(filter._config.className.auction)[0].innerHTML = '';
            renderArray.map(item =>
                auction.renderLot(
                    item.timeToFinish,
                    item.caption,
                    item.image.url,
                    item.image.alt,
                    item.price,
                    item.currentBid,
                    item.currentWinner,
                    item._id
                )
            );
            auction.startAuctionTimer(renderArray);
        }
    },
    clearFilter() {
        const radioButtons = document.getElementsByClassName(filter._config.checked.radioButton);
        const checkBoxes = document.getElementsByClassName(filter._config.checked.checkBox);
        const search = document.getElementsByClassName(filter._config.className.search)[0];
        for (let i = 0; i < radioButtons.length; i += 1) {
            if (radioButtons[i].checked) {
                radioButtons[i].checked = false;
            }
        }
        for (let i = 0; i < checkBoxes.length; i += 1) {
            if (checkBoxes[i].checked) {
                checkBoxes[i].checked = false;
            }
        }
        search.value = '';
    }
};

const addLots = {
    _config: {
        id: {
            dialog: 'addLotsDialog',
            sendButton: 'addLotsToAuctionButton'
        },
        classNames: {
            wrapperToStartAdding: 'add-lots-wrapper'
        },
        name: {
            form: 'lotsAddingForm'
        }
    },
    toggleAddLotsButton() {
        if (document.getElementsByClassName(addLots._config.classNames.wrapperToStartAdding)[0].style.display) {
            document.getElementsByClassName(addLots._config.classNames.wrapperToStartAdding)[0].style.display = '';
        } else {
            document.getElementsByClassName(addLots._config.classNames.wrapperToStartAdding)[0].style.display = 'flex';
        }
    },
    toggleDialogPopUp(e, display = true) {
        if (e) {
            e.stopPropagation();
        }
        if (!display || document.getElementById(addLots._config.id.dialog).style.display) {
            document.getElementById(addLots._config.id.dialog).style.display = '';
            document.removeEventListener('click', addLots.closeDialogClickOutTheBox);
            document
                .getElementById(addLots._config.id.sendButton)
                .removeEventListener('click', addLots.sendNewLotToServer);
        } else {
            document.getElementById(addLots._config.id.dialog).style.display = 'flex';
            document.addEventListener('click', addLots.closeDialogClickOutTheBox);
            document
                .getElementById(addLots._config.id.sendButton)
                .addEventListener('click', addLots.sendNewLotToServer);
        }
    },
    sendNewLotToServer(e) {
        function showError(text, parentElem) {
            const p = document.createElement('p');
            p.className = login._config.classNames.errorMessage;
            p.textContent = text;
            parentElem.appendChild(p);
            setTimeout(() => {
                parentElem.removeChild(p);
            }, 2000);
        }

        e.preventDefault();
        const formDataObj = new FormData(document.forms[addLots._config.name.form]);
        axios
            .post('/addLotsToAuction', formDataObj, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
            .then(response => {
                if (response.data.errorAuctionMsg) {
                    showError(response.data.errorAuctionMsg, document.forms[addLots._config.name.form]);
                } else {
                    addLots.toggleDialogPopUp(null, false);
                    pagination.goToLastPage();
                }
            })
            .catch(error => {
                showError(error.message, document.forms[addLots._config.name.form]);
            });
    },
    closeDialogClickOutTheBox(e) {
        if (!document.getElementById(addLots._config.id.dialog).contains(e.target)) {
            addLots.toggleDialogPopUp(e, true);
        }
    },
    changeTextContentToPath(e) {
        e.target.previousElementSibling.textContent = e.target.value.slice(e.target.value.lastIndexOf('\\') + 1);
    }
};

// actions initialize
const actions = {
    currentSlide: slider(),
    ...menu(),
    liftDots,
    auction,
    filter,
    login,
    pagination,
    addLots
};

export default actions;

window.onerror = function ownErrorHandler(errorMsg, url, lineNumber) {
    console.error(`Oops, an error has been occurred:\n${errorMsg} in ${url} on the ${lineNumber}`);
};

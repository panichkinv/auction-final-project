import Gl from './serviceVariables';
import actions from './actions';

const socket = io();
socket.on('connect', () => {
    socket.on('lots', data => {
        actions.auction.initialfillAuctionLots(data);
    });
    socket.on('serverDate', serverDate => {
        Gl.serverDate = serverDate;
    });
});
window.addEventListener('focus', () => socket.connect());

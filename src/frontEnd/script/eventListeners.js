import actions from './actions';

(function menu() {
    document.getElementById('categories').addEventListener('mouseenter', actions.openMenu);
    document.getElementById('categories').addEventListener('mouseleave', actions.closeMenu);
    document.getElementById('categoriesSubMenu').addEventListener('mouseenter', actions.openMenu);
    document.getElementById('categoriesSubMenu').addEventListener('mouseleave', actions.closeMenu);

    document.getElementById('mobileMenu').addEventListener('click', actions.toggleMobileMenu);
    document.getElementsByClassName('mobile-nav')[0].addEventListener('click', actions.toggleMobileMenu);
    document.getElementById('mobileCategory').addEventListener('click', actions.toggleMobileSubMenu);

    document.getElementsByClassName('header-wrapper')[0].addEventListener('click', actions.login.toggleLoginPopUp);
})();

(function loginInit() {
    document.getElementsByClassName('dialog-popup')[0].addEventListener('click', actions.login.signIn);
    document.addEventListener('DOMContentLoaded', actions.login.trySignInWithSession);
})();

(function slider() {
    document.getElementsByClassName('nav-dots-wrapper')[0].addEventListener('click', actions.currentSlide);
    document.getElementsByClassName('mainInfo')[0].addEventListener('mouseleave', actions.liftDots);
    document.getElementsByClassName('mainInfo')[0].addEventListener('mouseenter', actions.liftDots);
})();

(function filterInit() {
    document.getElementsByClassName('radio-wrapper')[0].addEventListener('click', actions.filter.applyFilter);
    document.getElementsByClassName('checkbox-wrapper')[0].addEventListener('click', actions.filter.applyFilter);
    document.getElementsByClassName('checkbox-wrapper')[1].addEventListener('click', actions.filter.applyFilter);
    document
        .getElementsByClassName('search-wrapper__search-field')[0]
        .addEventListener('input', actions.filter.applyFilter);
})();

(function auctionInit() {
    document.getElementsByClassName('auction-wrapper')[0].addEventListener('click', actions.auction.bidMoney);
})();

(function addLotsInit() {
    document
        .querySelector('.dialog-popup__input[type="file"]')
        .addEventListener('change', actions.addLots.changeTextContentToPath);
    document.getElementsByClassName('add-lots-wrapper')[0].addEventListener('click', actions.addLots.toggleDialogPopUp);
})();

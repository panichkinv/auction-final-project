(function getAncestorByClassNamePolyfill() {
    if (!Element.prototype.getAncestorByClassName) {
        Element.prototype.getAncestorByClassName = function(className) {
            let currentParent = this.parentElement;
            while (true) {
                if (currentParent === null) {
                    return null;
                }

                if (currentParent.classList.contains(className)) {
                    return currentParent;
                }

                currentParent = currentParent.parentElement;
            }
        };
    }
})();

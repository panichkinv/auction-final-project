const path = require('path');
const config = require('config');
const webpack = require('webpack');

module.exports = {
    entry: {
        app: [
            './src/frontEnd/script/errorHandling.js',
            './src/frontEnd/script/eventListeners.js',
            './src/frontEnd/script/polyfill.js',
            './src/frontEnd/script/socketActions.js',
            './src/frontEnd/script/actions.js'
        ]
    },
    output: {
        path: path.resolve(__dirname, './src/frontEnd/bundle'),
        filename: 'bundle.js'
    },
    devtool: 'source-maps',
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            [
                                'env',
                                {
                                    targets: {
                                        browsers: ['last 2 versions', 'ie >= 11']
                                    }
                                }
                            ],
                            ['es2015-ie']
                        ],
                        plugins: [
                            require('babel-plugin-transform-object-rest-spread'),
                            'transform-es2015-arrow-functions',
                            'es6-promise'
                        ]
                    }
                }
            }
        ]
    }
};

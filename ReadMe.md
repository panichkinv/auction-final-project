# Auction

Auction project is a final task of JavaScript | FrontEnd courses

### Installing

Install [mongoDB](https://www.mongodb.com/download-center?jmp=nav#community) and start daemon with the command

```
mongod --dbpath ~/path/to/your/app/data 
```

Check connection by opening another CMD with command

```
mongo
```

Configure config/default.json file

* Server host and port
* Database url and dbName
* Session secret
* Destination for lots' images when user will be adding new one


## Getting Started

To start a project - run console in a project direcory with a command:

```npm start```

After that mongodb and node processes will be launched

## Running the tests

```
npm test
```

## Documentation

Basic documentation is available with by a local path __/docs__ 

For example

```
http://localhost:8020/docs
```

## Deployment

In case of production deployment you should pre-install webpack v4 and generate production build with the command:
```
npm run prodBuild
```

## Built With

* [Node.js](https://nodejs.org/en/) - JavaScript server
* [Express](http://expressjs.com/) - Web framework for Node.js
* [Babel](http://babeljs.io/) - JavaScript compiler
* [Webpack](https://webpack.js.org/) - Javascript bundler



## Authors

* **Panichkin Viktor** - *Initial work* - [panichkinv](https://bitbucket.org/panichkinv/)


## License

This project is licensed under the MIT License
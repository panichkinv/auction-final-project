const http = require('http');
const path = require('path');
const express = require('express');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const bodyParser = require('body-parser');
const config = require('config');
const multer = require('multer');

const DB = require('./src/backEnd/MongoDB/connectToDB');
const routes = require('./src/backEnd/routes');

const upload = multer({
    storage: multer.diskStorage({
        destination(req, file, cb) {
            cb(null, config.get('Lots.images.destination'));
        },
        filename(req, file, cb) {
            cb(null, `${Date.now()}-${file.fieldname}${file.originalname.slice(file.originalname.lastIndexOf('.'))}`);
        }
    })
});

const app = express();
const port = config.get('Server.port');

app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(
    session({
        secret: config.get('Session.secret'),
        store: new MongoStore({ url: `${config.get('Database.url')}/${config.get('Database.dbName')}` }),
        resave: false,
        saveUninitialized: false
    })
);
app.use(express.static(path.join(__dirname, './src/frontend')));
app.use('/docs', express.static(`${__dirname}/docs`));

function startServer(callback) {
    DB.connectDB()
        .then(() => {
            const server = http.createServer(app);
            server.listen(process.env.PORT || port);
            console.log(`Auction is working on port ${process.env.PORT || port}`);
            routes(app, server, upload);
            if (callback) {
                callback();
            }
        })
        .catch(e => console.error(e));
}

if (module.parent) {
    module.exports.startServer = startServer;
} else {
    startServer();
}
